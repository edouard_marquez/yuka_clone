package com.example.myapplication.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

// TODO Ajouter les éléments manquants
@Parcelize
data class Product(val name: String) : Parcelable