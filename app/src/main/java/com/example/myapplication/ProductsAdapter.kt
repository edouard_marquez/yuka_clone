package com.example.myapplication

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.model.Product
import kotlinx.android.synthetic.main.list_item_cell.view.*

// Un adapter permet d'indiquer quel est le contenu d'une liste
// Ici, on dit que l'on affiche une liste de produits
class ProductsAdapter(val products: List<Product>, val listener: OnProductItemClicked) : RecyclerView.Adapter<ProductItemCell>() {

    // On explique comment créer une cellule
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductItemCell {
        return ProductItemCell(
                LayoutInflater.from(parent.context)
                        .inflate(R.layout.list_item_cell, parent, false)
        )
    }

    // On explique comment afficher le contenu de la cellule à la [position]
    override fun onBindViewHolder(holder: ProductItemCell, position: Int) {
        val product = products[position]
        holder.bindProduct(product)
        holder.itemView.setOnClickListener {
            listener.onProductClicked(product)
        }
    }

    // Le nombre d'éléments à afficher dans la liste
    override fun getItemCount(): Int {
        return products.count()
    }

}

// Classe représentant une cellule
class ProductItemCell(v: View) : RecyclerView.ViewHolder(v) {

    // Les différentes vues qui composent la cellule
    private val name: TextView = v.product_name

    // Comment afficher le produit dans une cellule
    fun bindProduct(product: Product) {
        name.text = product.name
    }

}

interface OnProductItemClicked {
    fun onProductClicked(product: Product)
}