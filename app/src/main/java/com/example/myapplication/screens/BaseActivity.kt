package com.example.myapplication.screens

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.example.myapplication.R

// Activité générique qui gère :
// - L'arrière-plan avec dégradé sur la Toolbar
// - Le bouton retour sur la Toolbar
abstract class BaseActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.setBackgroundDrawable(ContextCompat.getDrawable(this, R.drawable.bg_toolbar))

        // La Toolbar affiche un flèche retour
        if (showBackButton()) {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        // Quand on clique sur la flèche retour, on ferme l'écran
        if (item?.itemId == android.R.id.home) {
            finish()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    // Méthode utilitaire qui indique s'il faut afficher le bouton Retour dans la Toolbar
    open fun showBackButton() : Boolean {
        return false
    }

}