package com.example.myapplication.screens.details

import android.os.Bundle
import com.example.myapplication.model.Product
import com.example.myapplication.screens.BaseActivity

// Ecran affichant les détails
class DetailsProductActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // Récupérer le produit passé en paramètre
        val product = intent.getParcelableExtra<Product>("product")

        if (product == null) {
            finish()
            return
        }

        // TODO Afficher le layout de détail
        //setContentView(R.layout.xxx)

        // TODO Utiliser le product dans la vue
    }

    override fun showBackButton(): Boolean {
        return true
    }
}