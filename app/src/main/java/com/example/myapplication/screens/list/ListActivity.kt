package com.example.myapplication.screens.list

import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myapplication.R
import com.example.myapplication.screens.BaseActivity
import kotlinx.android.synthetic.main.list.*

class ListActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.list)
        products_list.layoutManager = LinearLayoutManager(this)

        // 1) Liste de faux produits à créer
        // 2) Donner cette liste à l'Adapter

        /*products_list.adapter = ProductsAdapter(products,
                object : OnProductItemClicked {
                    override fun onProductClicked(product: Product) {
                        val intent = Intent(this@ListActivity, DetailsProductActivity::class.java)
                        intent.putExtra("product", product)
                        startActivity(intent)
                    }
                })*/
    }

    // Le menu (= les icônes) à utiliser pour la Toolbar
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_main_activity, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == R.id.barcode) {
            // TODO Ouvrir le lecteur de code-barres
            return true
        }
        return super.onOptionsItemSelected(item)
    }
}